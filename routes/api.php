<?php
//header('Access-Control-Allow-Origin:  *');
//header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
//header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function(){
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/show', 'RecipeController@show');

    Route::get('/test', 'RecipeController@test');

    Route::post('/delete-recipe/{recipe}', 'RecipeController@destroy');

    Route::post('/logout', 'AuthController@logout');
    Route::post('/add-recipe', 'RecipeController@store');
    Route::post('/edit-recipe/{recipe}', 'RecipeController@update');

    Route::get('/generate-recipe-list/{length}', 'RecipeController@generate');


    Route::get('/recipe/{recipe}', 'RecipeController@index');

//    Ingredients
    Route::post('/add-ingredient', 'IngredientsController@store');
    Route::get('/get-ingredients', 'IngredientsController@show');
    Route::post('/edit-ingredient/{ingredients}', 'IngredientsController@update');

    Route::post('/delete-ingredient/{ingredients}', 'IngredientsController@destroy');

});


Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');


