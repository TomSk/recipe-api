import HomePage from './components/pages/HomePage';
import LoginPage from './components/pages/LoginPage';
import RegisterPage from './components/pages/RegisterPage';
import LogoutPage from './components/pages/LogoutPage';
import RecipePage from './components/pages/RecipePage';
import MyAccountPage from './components/pages/MyAccountPage';

const routes = [
    {
        path: '/',
        component: HomePage
    },
    {
        path: '/login',
        name: 'login',
        component: LoginPage,
        meta: {
            requiresVisitor: true
        }
    },
    {
        path: '/register',
        name: 'register',
        component: RegisterPage,
        meta: {
            requiresVisitor: true
        }
    },
    {
        path: '/logout',
        name: 'logout',
        component: LogoutPage
    },
    {
        path: '/recipes',
        name: 'recipes',
        component: RecipePage,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/account',
        name: 'account',
        component: MyAccountPage,
        meta: {
            requiresAuth: true
        }
    }
];

export default routes;