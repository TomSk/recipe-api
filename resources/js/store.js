import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import ls from 'local-storage';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: ls.get('token') || null,
        recipes:[]
    },
    getters:{
        loggedIn: (state) => () => {
            return state.token !== null;
        }
    },
    mutations: {
        SET_RECIPIES(state, payload){
            state.recipes = payload;
        },
        SET_TOKEN(state, payload){
            state.token = payload;
        },
        DESTROY_TOKEN(state, payload){
            state.token = null;
        },
        CLEAR(state){
            state.recipes = [];
        }
    },
    actions: {
        fetchData(context){

        },
        getRecipes(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;

            return new Promise((resolve, reject) => {
                axios.get('/api/show').then((res) => {
                    context.commit('SET_RECIPIES', res.data);
                    resolve(res);
                }).catch(error => {
                    console.log(error);
                    reject(error)
                })
            });
        },
        retrieveToken(context, payload){
            return new Promise((resolve, reject) => {
                axios.post('/api/login', {
                    username: payload.email,
                    password: payload.password
                }).then((res) => {

                    const token = res.data.access_token;

                    ls.set('token', token);

                    context.commit('SET_TOKEN', token);

                    resolve(res);

                }).catch(error => {
                    console.log(error);
                    reject(error)
                });
            });
        },
        register(context, payload){
            return new Promise((resolve, reject) => {
                axios.post('/api/register', {
                    name: payload.name,
                    email: payload.email,
                    password: payload.password
                }).then((res) => {

                    // context.dispatch('retrieveToken', {
                    //     username: res.email,
                    //     password: payload.password
                    // })
                    resolve(res);
                }).catch(error => {
                    console.log(error);
                    reject(error)
                });
            });
        },
        destroyToken(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;

            if(context.getters.loggedIn()){
                return new Promise((resolve, reject) => {
                    axios.post('/api/logout')
                        .then((res) => {

                        ls.remove('token');

                        context.commit('DESTROY_TOKEN');

                        resolve(res);

                    }).catch(error => {
                        console.log(error);
                        reject(error)
                    });
                });
            }
        },
        editRecipe(context, payload){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;

            if(context.getters.loggedIn()){
                return new Promise((resolve, reject) => {
                    axios.post('/api/edit-recipe/' + payload.id, payload)
                        .then((res) => {
                            context.dispatch('getRecipes').then(() => {
                                resolve(res);
                            });

                        }).catch(error => {
                            console.log(error);
                            reject(error)
                    });
                });
            }
        }
    }
});