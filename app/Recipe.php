<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'prep_time',
        'cook_time',
        'servings',
        'ingredients',
        'method',
        'tags',
        'rating'
    ];

    protected  $hidden = ['updated_at'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}
