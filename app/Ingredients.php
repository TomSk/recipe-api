<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'price',
    ];

    protected  $hidden = ['updated_at'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}
