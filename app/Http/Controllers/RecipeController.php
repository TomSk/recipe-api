<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Auth;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Recipe $recipe)
    {
        //

        $recipe['ingredients'] = unserialize($recipe['ingredients']);
        $recipe['method'] = unserialize($recipe['method']);
        $recipe['tags'] = unserialize($recipe['tags']);

        return $recipe;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $data = $request->validate([
            'title' => 'required|string',
            'description' => 'string',
            'prepTime' => 'required|string',
            'cookTime' => 'required|string',
            'servings' => 'required|string',
            'ingredients' => 'required|array',
            'method' => 'required|array',
            'tags' => 'array',
            'rating' => 'required|integer'
        ]);

        $recipe = Recipe::create([
            'user_id'       => auth()->user()->id,
            'title'         => $request['title'],
            'description'   => $request['description'],
            'prep_time'     => $request['prepTime'],
            'cook_time'     => $request['cookTime'],
            'servings'      => $request['servings'],
            'rating'        => $request['rating'],
            'ingredients'   => serialize($request['ingredients']),
            'method'        => serialize($request['method']),
            'tags'          => serialize($request['tags'])
        ]);
//

        return response( $recipe, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        //

        $recipes = Recipe::where('user_id', auth()->user()->id)->get();

        $recipes->map(function($r) {
            $r->ingredients = unserialize($r->ingredients);
            return $r;
        });

        return $recipes;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        //

        $data = $request->validate([
            'title' => 'required|string',
            'description' => 'string',
            'prepTime' => 'required|string',
            'cookTime' => 'required|string',
            'servings' => 'required|string',
            'ingredients' => 'required|array',
            'method' => 'required|array',
            'tags' => 'array',
            'rating' => 'required|integer'
        ]);


        $recipe->update([
            'title'         => $request['title'],
            'description'   => $request['description'],
            'prep_time'     => $request['prepTime'],
            'cook_time'     => $request['cookTime'],
            'servings'      => $request['servings'],
            'rating'        => $request['rating'],
            'ingredients'   => serialize($request['ingredients']),
            'method'        => serialize($request['method']),
            'tags'          => serialize($request['tags'])
        ]);

        return response($recipe, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        //

        $recipe->delete();
    }


    public function generate($length, Recipe $recipe)
    {
        $recipesAll = $recipe->all();
        $recipeCount = count($recipesAll);
        $collection = collect($recipesAll);
        $diff = null;

        if($length > $recipeCount) {


            $random = $collection->random($recipeCount);

            $list = $random->map(function($r) {
                if(gettype($r->ingredients) == 'string'){
                    $r->ingredients = unserialize($r->ingredients);
                }
                return $r;
            });

            $arr = $list->all();

            $result = array();

            for ($x = 0; $x <= ceil($length/count($arr)); $x++) {
                $result = array_merge($result, $list->all());
            }


            return array_slice($result,0, $length);
//            return [
//                '$length' => $length,
//                'count' => count($result, 0, $length)
//            ];

        } else {
            $random = $collection->random($length);

            $list = $random->map(function($r) {
                $r->ingredients = unserialize($r->ingredients);
                return $r;
            });

            return $list->all();
        }



    }
}
