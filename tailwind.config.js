module.exports = {
    theme: {
        screens: {
          xs: '640px',
          sm: '750px',//'640px',
          md: '992px',//'768px',
          lg: '1200px',//'1024px',
          xl: '1640px'//'1280px',
        },
    extend: {
        spacing: {
            '18': '4.5rem'
        }
    }
    },
    variants: {},
    plugins: []
}
